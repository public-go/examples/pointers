package pointers

// SomeType has an a and b as int
type SomeType struct{ a, b int }

func createSomePointerP() *SomeType {
	p := new(SomeType)
	p.a = 3
	p.b = 4

	return p
}

func createSomePointerQ() *SomeType {

	q := &SomeType{3, 4}
	return q
}
