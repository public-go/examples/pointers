package interfaces

// Sorter interface defines convenience functions for sorting
type Sorter interface {
	Len() int
	Less(i, j int) bool
	Swap(i, j int)
}

// Xi is a slice of int type
type Xi []int

// Len for ints
func (p Xi) Len() int {
	return len(p)
}

// Less for ints
func (p Xi) Less(i, j int) bool {
	return p[i] < p[j]
}

// Swap ints
func (p Xi) Swap(i, j int) {
	p[i], p[j] = p[j], p[i]
}

// Xs is a slice of string type
type Xs []string

// Less for strings
func (p Xs) Less(i, j int) bool {
	return p[i] < p[j]
}

// Len for strings
func (p Xs) Len() int {
	return len(p)
}

// Swap strings
func (p Xs) Swap(i, j int) {
	p[i], p[j] = p[j], p[i]
}

// Sort does sort an slice
func Sort(x Sorter) {
	for i := 0; i < x.Len(); i++ {
		for j := i + 1; j < x.Len(); j++ {
			if !x.Less(i, j) {
				x.Swap(i, j)
			}
		}
	}
}
