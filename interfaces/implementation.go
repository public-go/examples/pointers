// Package interfaces shows things you can do with interfaces
package interfaces

import "net/http"

// MyResponse holds essential parts of the http.Response in which I'm interested.
type MyResponse struct {
	status, method, url string
}

// CreateMyResponse inits MyResponse
func CreateMyResponse(response *http.Response) (r *MyResponse) {
	return &MyResponse{method: response.Request.Method, status: response.Status, url: response.Request.URL.String()}
}

// ReturnStatus returns status in a nice formatted string
func (r *MyResponse) ReturnStatus() string {
	return r.method + " to " + r.url + " returned " + r.status
}

// UrlString returns requested url
func (r *MyResponse) UrlString() string {
	return "Url: " + r.url
}
