package pointers

import "testing"

const (
	wantA = 3
	wantB = 4
)

func TestP(t *testing.T) {
	got := createSomePointerP()
	if got.a != wantA || got.b != wantB {
		t.Errorf("Some trouble with P, got a: %v, b: %v, want a: %v b: %v", got.a, got.b, wantA, wantB)
	}
}

// func TestQ(t *testing.T) {
// 	got := createSomePointerQ()
// 	if got.a != wantA || got.b != wantB {
// 		t.Errorf("Some trouble with Q, got a: %v, b: %v, want a: %v b: %v", got.a, got.b, wantA, wantB)
// 	}
// }
