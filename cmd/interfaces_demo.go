package main

import (
	"log"

	"gitlab.com/public-go/examples/pointers/interfaces"
)

func runInterfacesExample() {
	stringValues := interfaces.Xs{"c", "a", "d", "b"}
	log.Println("Before sort: ", stringValues)
	interfaces.Sort(stringValues)
	log.Println("After sort: ", stringValues)

	intValues := interfaces.Xi{1, 4, 2, 6, 0, 10}
	log.Println("Before sort: ", intValues)
	interfaces.Sort(intValues)
	log.Println("After sort: ", intValues)

}
