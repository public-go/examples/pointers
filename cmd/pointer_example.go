package main

import "fmt"

func playWithPointers() {
	i := 4
	fmt.Println(i)
	fmt.Println(&i)

	// var j int
	// j = 4
	j := &i
	fmt.Println(*j)
	fmt.Println(j)
	fmt.Println(&j)

	*j = 5
	fmt.Println(i)
}
