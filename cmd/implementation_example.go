package main

import (
	"fmt"
	"net/http"

	"gitlab.com/public-go/examples/pointers/interfaces"
)

func ImplementationDemo() {
	resp, _ := http.Get("https://google.com")
	myresp := interfaces.CreateMyResponse(resp)
	fmt.Println(myresp.ReturnStatus())
	fmt.Println(myresp.UrlString())

}
