# How to run examples

run pointer test: `go test -v -cover`

run interfaces and implemntation example demo: 
```bash
cd cmd
go build -o example .
./example
```